package io.cl.example.rabbitmqexamplespringboot.configuration;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class RabbitConfig {

    public static final String HTTP_REQUEST_NORMAL_EXCHANGE_NAME = "http.request.direct";
    public static final String HTTP_REQUEST_DEAD_LETTER_EXCHANGE_NAME = "http.request.dlx.direct";
    public static final String HTTP_REQUEST_NORMAL_QUEUE_NAME = "http.request.queue";
    public static final String HTTP_REQUEST_DEAD_LETTER_QUEUE_NAME = "http.request.dlx.queue";
    public static final String HTTP_REQUEST_RETRY_ROUTING_KEY = "http.request.retry.routing";

    @Bean
    public Queue httpRequestQueue() {
        Map<String, Object> args = new HashMap<>();
        args.put("x-message-ttl", 30000);
        args.put("x-dead-letter-exchange", HTTP_REQUEST_DEAD_LETTER_EXCHANGE_NAME);
        args.put("x-dead-letter-routing-key", HTTP_REQUEST_RETRY_ROUTING_KEY);
        return new Queue(HTTP_REQUEST_NORMAL_QUEUE_NAME, false, false, false, args);
    }

    @Bean
    DirectExchange httpRequestExchange() {
        return new DirectExchange(HTTP_REQUEST_NORMAL_EXCHANGE_NAME);
    }

    @Bean
    Binding httpRequestBinding(Queue httpRequestQueue, DirectExchange httpRequestExchange) {
        return BindingBuilder.bind(httpRequestQueue).to(httpRequestExchange).with(HTTP_REQUEST_RETRY_ROUTING_KEY);
    }

    @Bean
    public Queue httpRequestDlxQueue() {
        Map<String, Object> args = new HashMap<>();
        return new Queue(HTTP_REQUEST_DEAD_LETTER_QUEUE_NAME, false, false, false, args);
    }

    @Bean
    DirectExchange httpRequestDlxExchange() {
        return new DirectExchange(HTTP_REQUEST_DEAD_LETTER_EXCHANGE_NAME);
    }

    @Bean
    Binding httpRequestDlxBinding(Queue httpRequestDlxQueue, DirectExchange httpRequestDlxExchange) {
        return BindingBuilder.bind(httpRequestDlxQueue).to(httpRequestDlxExchange).with(HTTP_REQUEST_RETRY_ROUTING_KEY);
    }
}
