package io.cl.example.rabbitmqexamplespringboot.bean;

import lombok.*;

import java.io.Serializable;

/**
 * 用于RabbitMQ生产者和消费者交换数据的封装Bean
 */
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class HttpRequestBean implements Serializable {

    /**
     * HTTP Method, 可能包含的值为: GET, POST, PUT等
     * 注意: 只能使用大写字母
     */
    @Getter
    @Setter
    private String method;

    /**
     * 一个合法的HTTP请求链接
     */
    @Getter
    @Setter
    private String url;

    /**
     * 一个合法的JSON格式数据体
     */
    @Getter
    @Setter
    private String body;

}
