package io.cl.example.rabbitmqexamplespringboot.bean;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

/**
 * 用于HTTP Response响应到终端的封装Bean
 */
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class MessageBodyBean {

    /**
     * 消息码: 与HTTP状态码相同含义, 默认200为成功, 404为无法找到资源, 401为权限受限等等
     */
    @Getter
    @Setter
    private Integer messageCode = 200;

    /**
     * 消息文本: 默认为OK, 其它情况以描述为准
     */
    @Getter
    @Setter
    private String messageText = "OK";

    /**
     * 消息数据: 任意数据结构, 最后被jackson序列为JSON数据响应回客户端
     */
    @Getter
    @Setter
    private List<Object> messageData = new ArrayList<>();

}
