package io.cl.example.rabbitmqexamplespringboot.controller;

import io.cl.example.rabbitmqexamplespringboot.bean.MessageBodyBean;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * 一个仅限于测试HTTP GET和POST参数的控制器
 */
@RestController
@RequestMapping("/index")
@Slf4j
public class IndexController {

    /**
     * 示例HTTP GET请求: http://localhost:8080/httpqueue/sendMessage, 返回一个JSON
     *
     * @param messageContent 测试接受字符串请求的参数
     * @return 封装消息体
     */
    @GetMapping("/getText")
    public MessageBodyBean getGetText(String messageContent) {
        log.info("GET Request value: {}", messageContent);
        return new MessageBodyBean();
    }

    /**
     * 示例HTTP POST请求: http://localhost:8080/httpqueue/sendMessage, 返回一个JSON
     *
     * @param messageContent 测试接受字符串请求的参数
     * @return 封装消息体
     */
    @PostMapping("/getText")
    public MessageBodyBean getPostText(@RequestBody String messageContent) {

        log.info("POST Request value: {}", messageContent);
        return new MessageBodyBean();
    }

}
