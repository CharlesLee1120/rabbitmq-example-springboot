package io.cl.example.rabbitmqexamplespringboot.controller;

import io.cl.example.rabbitmqexamplespringboot.bean.MessageBodyBean;
import io.cl.example.rabbitmqexamplespringboot.service.HttpRequestProducerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/httpqueue")
@Slf4j
public class HttpRequestController {

    @Autowired
    private HttpRequestProducerService httpRequestProducerService;

    @GetMapping(value = "/sendMessage")
    public MessageBodyBean sendGetMessage(HttpServletRequest request, @RequestParam String url) {

        // Get http request method, here it's "GET"
        String requestMethod = request.getMethod();

        log.info("HttpRequest parameter, Method: {}, URL: {}", requestMethod, url);

        if (StringUtils.hasText(url)) {
            this.httpRequestProducerService.sendMessage(requestMethod, url);
        }

        return new MessageBodyBean();
    }

    @PostMapping("/sendMessage")
    public MessageBodyBean sendPostMessage(HttpServletRequest request, @RequestParam String url, @RequestParam String body) {

        // Get http request method, here it's "POST"
        String requestMethod = request.getMethod();

        log.info("HttpRequest parameter, Method: {}, URL: {}, Body: {}", requestMethod, url, body);

        if (StringUtils.hasText(url)) {
            this.httpRequestProducerService.sendMessage(requestMethod, url, body);
        }

        return new MessageBodyBean();
    }

}
