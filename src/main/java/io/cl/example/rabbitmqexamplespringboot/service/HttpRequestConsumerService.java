package io.cl.example.rabbitmqexamplespringboot.service;

import com.rabbitmq.client.Channel;
import io.cl.example.rabbitmqexamplespringboot.bean.HttpRequestBean;
import io.cl.example.rabbitmqexamplespringboot.configuration.RabbitConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

@Service
@Slf4j
public class HttpRequestConsumerService {

    @Autowired
    private RestTemplate restTemplate;

    @RabbitListener(queues = RabbitConfig.HTTP_REQUEST_NORMAL_QUEUE_NAME)
    public void receiveMessage(Message message, Channel channel) {

        ObjectInputStream inputStream = null;
        try {
            inputStream = new ObjectInputStream(new ByteArrayInputStream(message.getBody()));

            if (inputStream != null) {

                HttpRequestBean httpRequestBean = (HttpRequestBean) inputStream.readObject();

                log.info("HttpRequestBean Deserializable: {}", httpRequestBean.toString());

                if (httpRequestBean != null) {
                    try {
                        String postForObject = null;
                        if (httpRequestBean.getMethod().equals("GET")) {

                            postForObject = restTemplate.getForObject(httpRequestBean.getUrl(), String.class);
                            log.info("Send HTTP GET Request: {}", httpRequestBean.getUrl());

                        } else if (httpRequestBean.getMethod().equals("POST")) {

                            HttpHeaders httpHeaders = new HttpHeaders();
                            httpHeaders.setContentType(MediaType.valueOf("application/json; charset=utf-8"));
                            HttpEntity<String> httpEntity = new HttpEntity<>(httpRequestBean.getBody(), httpHeaders);

                            postForObject = restTemplate.postForObject(httpRequestBean.getUrl(), httpEntity, String.class);
                            log.info("Send HTTP POST Request: {}", httpRequestBean.getUrl());
                        }

                        if (StringUtils.hasText(httpRequestBean.getUrl())) {
                            channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
                            log.info("Ack RabbitMQ Message, Topic: {}, URL: {}, Status: Ack, Body: {}", RabbitConfig.HTTP_REQUEST_NORMAL_QUEUE_NAME, httpRequestBean.getUrl(), postForObject);
                        } else {
                            channel.basicNack(message.getMessageProperties().getDeliveryTag(), false, false);
                            log.info("Nack RabbitMQ Message, Topic: {}, URL: {}, Status: Nack", RabbitConfig.HTTP_REQUEST_NORMAL_QUEUE_NAME, httpRequestBean.getUrl());
                        }

                    } catch (IOException e) {
                        log.info("Exception RabbitMQ Message, Topic: {}, URL: {}, Status: Exception", RabbitConfig.HTTP_REQUEST_NORMAL_QUEUE_NAME, httpRequestBean.getUrl());
                        e.printStackTrace();
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }
}
