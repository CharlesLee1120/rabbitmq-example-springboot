package io.cl.example.rabbitmqexamplespringboot.service;

import io.cl.example.rabbitmqexamplespringboot.bean.HttpRequestBean;
import io.cl.example.rabbitmqexamplespringboot.configuration.RabbitConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class HttpRequestProducerService {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void sendMessage(String method, String url) {

        HttpRequestBean httpRequestBean = new HttpRequestBean(method, url, null);

        log.info("HttpRequestBean Serializable: {}", httpRequestBean);

        this.rabbitTemplate.convertAndSend(RabbitConfig.HTTP_REQUEST_NORMAL_EXCHANGE_NAME, RabbitConfig.HTTP_REQUEST_RETRY_ROUTING_KEY, httpRequestBean);

        log.info("Send RabbitMQ Message, Method: {}, URL: {}", method, url);

        this.rabbitTemplate.setConfirmCallback((correlationData, ack, cause) -> {
            log.info("Producer Callback invoke. correlationData: {}, ACK: {}", correlationData, ack);
        });
    }

    public void sendMessage(String method, String url, String body) {

        HttpRequestBean httpRequestBean = new HttpRequestBean(method, url, body);

        log.info("HttpRequestBean Serializable: {}", httpRequestBean);

        this.rabbitTemplate.convertAndSend(RabbitConfig.HTTP_REQUEST_NORMAL_EXCHANGE_NAME, RabbitConfig.HTTP_REQUEST_RETRY_ROUTING_KEY, httpRequestBean);

        log.info("Send RabbitMQ Message, Method: {}, URL: {}", method, url);

        this.rabbitTemplate.setConfirmCallback((correlationData, ack, cause) -> {
            log.info("Producer Callback invoke.");
        });
    }

}
