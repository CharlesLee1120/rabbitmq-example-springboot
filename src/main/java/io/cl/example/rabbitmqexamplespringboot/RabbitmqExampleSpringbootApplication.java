package io.cl.example.rabbitmqexamplespringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RabbitmqExampleSpringbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(RabbitmqExampleSpringbootApplication.class, args);
	}

}
