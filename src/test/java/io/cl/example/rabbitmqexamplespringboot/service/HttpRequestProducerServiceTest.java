package io.cl.example.rabbitmqexamplespringboot.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class HttpRequestProducerServiceTest {

    @Autowired
    HttpRequestProducerService httpRequestProducerService;

    @Test
    public void sendMessage() {
        String url = "http://172.16.105.146:8083/";
        String method = "GET";

        httpRequestProducerService.sendMessage(method, url);
    }
}
